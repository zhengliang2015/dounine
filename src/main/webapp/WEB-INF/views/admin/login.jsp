<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://dounine.com/dounine-frame/csrf.tld" prefix="dounine-csrf" %>
<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="${base}/resource/favicon.ico" type="image/x-icon" />
<link href="${base_url}/resource/admin/css/login.css" rel="stylesheet"
	type="text/css" />
<script src="${base_url}/resource/admin/js/common/jquery.min.js"
	type="text/javascript" charset="utf-8"></script>
<script src="${base_url}/resource/admin/js/common/cloud.js"
	type="text/javascript" charset="utf-8"></script>
<title>逗你呢-后台登录系统</title>
</head>
<script language="javascript">
	$(function() {
		$('.loginbox').css({
			'position' : 'absolute',
			'left' : ($(window).width() - 692) / 2
		});
		$(window).resize(function() {
			$('.loginbox').css({
				'position' : 'absolute',
				'left' : ($(window).width() - 692) / 2
			});
		})
	});
</script>

<body style="background-color:#1c77ac; background-image:url(${base_url}/resource/admin/images/index/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">
	<div id="mainBody">
		<div id="cloud1" class="cloud"></div>
		<div id="cloud2" class="cloud"></div>
	</div>

	<div class="loginbody">

		<!-- <span class="systemlogo"></span> -->
		
		<div style="text-align:center;margin-top:5%;font-size:22px;color:#F2F2F2;">
			逗你呢(dounine) - 后台管理系统
		</div>

		<div class="loginbox">
			<form method="post" action="${base}/admin/login.html">
				<ul>
					<li><input type="text" class="loginuser" name="userName"
						value="admin" /></li>
					<li><input type="password" class="loginpwd"
						name="userPassword" value="admin" /></li>
					<li><input type="submit" class="loginbtn" value="登录" /><label><input
							name="rememberMe" type="checkbox" style="vertical-align:middle;" checked="checked" />记住密码</label><label style="color:red;">${errorMsg}</label></li>
				</ul>
			</form>

		</div>

	</div>



	<div class="loginbm">
		&copy 版权所有 2015 <a href="http://dounine.com">逗你呢(dounine) - 后台管理系统</a>
	</div>


</body>

</html>
